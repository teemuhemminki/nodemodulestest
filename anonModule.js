//Tehtävän c moduuli, joka palauttaa anonyymin olion jolla on kaksi metodia.

class NumberObject {
    constructor() {
        this.numbersArray = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    }

    randomNumber() {
        var randomNumber = Math.floor(Math.random() * 20);
        return randomNumber;
    }

    calcAverage() {
        var avg = 0;
        var i;

        for (i = 0; i < this.numbersArray.length; i++) {
            avg += this.numbersArray[i];
        }

        return (avg / this.numbersArray.length);
    }

};

//Exporttaa prototyypin
module.exports = NumberObject;