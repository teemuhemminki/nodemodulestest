//Command linessa mene NodeModule kansioon ja kutsu "node app.js" komentoa.

//Haetaan kaikki moduulit käyttöön
var rangeInt = require('./rangeint.js');
var numberObject = require('./classModule.js').NumberObject;
var anonObject = require('./anonModule.js');

//anonObjectista pitää luoda olio, jotta sitä voi käyttää.
var anon = new anonObject();

//Käytetään rangeInt moduulia
console.log('Rangeint returns: ' + rangeInt(10, 100));

//Käytetään numberObject moduulin tuomaa oliota
console.log('number object returns: ' + numberObject.randomNumber());
console.log('number object returns: ' + numberObject.calcAverage());

//Käytetään anonModulesta luotua oliota
console.log('anonymous object returns: ' + anon.randomNumber());
console.log('anonymous object returns: ' + anon.calcAverage());